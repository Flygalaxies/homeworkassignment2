/*
*   Main Entry point for the api
*/

//Dependencies
let server = require('./lib/server');
let helpers = require('./lib/helpers');


let app = {};

//Instantiate The server
app.init = () => {
    helpers.directoryCheck('.data');
    server.init();
}

//Execute the app
app.init();

module.exports = app;