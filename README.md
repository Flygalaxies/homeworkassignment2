# HomeworkAssignment2

A Small Pizza Delivery API where New users needs to be created, with a possibility to be edited and deleted once logged in.
The user should be able to log in to the app securely
The user can have administration rights in order to create, update and delete items.
The user must be able to add items to the checkout and pay with a receipt emailed to them.

Available Methods to call

<b>REGISTER A USER</b><br />
[POST].../auth/register<br />
--BODY:<br />
email(string),<br />
password(string)<br />
name(string)<br />
address(string)<br />
isAdmin(boolean)<br />
<b>LOGIN --Returns token</b><br />
[POST].../auth/login<br />
--BODY:<br />
email(string)<br />
password(string)<br />
<b>GET A LOGGED IN USER</b><br />
[GET].../user<br />
--QUERY:<br />
tokenId(string)<br />
<b>Delete a logged in user</b><br />
[DELEETE].../user<br />
--QUERY:<br />
tokenId(string)<br />
<b>Update a logged in user</b><br />
[PUT].../user<br />
--QUERY:<br />
tokenId(string)<br />
<b>Add a item (Must have isAdmin set to true)</b><br />
[POST].../items<br />
--QUERY:<br />
tokenId(string)<br />
--BODY:<br />
name(string)<br />
description(string)<br />
amount(number)<br />
<b>Get Every item</b><br />
[GET].../items/all<br />
--QUERY:<br />
tokenId(string)<br />
</b>Get a single item</b><br />
[GET].../items<br />
--QUERY:<br />
tokenId(string)<br />
itemName(string)<br />
<b>Update a item (At least one body parameter must be provided, otherwise optional)</b>
[PUT].../items<br />
--QUERY:<br />
tokenId(string)<br />
itemName(string)<br />
--BODY:<br />
name(string)<br />
description(string)<br />
amount(number)<br />
<b>Delete an item</b><br />
[DELETE].../items<br />
--QUERY<br />
tokenId(string)<br />
itemName(string)<br />
<b>Add item to checkout</b><br />
[POST].../checkout/addItem<br />
--QUERY<br />
tokenId(string)<br />
--BODY<br />
name(Array)--Item Names<br />
<b>Pay for the item/items in chekout</b><br />
[POST].../checkout/pay<br />
--QUERY<br />
tokenId(string)<br />
email(string)<br />
--BODY<br />
cardNumber(number)<br />
expMonth(number)--Must be 2 numbers<br />
expYear(number)--Must be 4 numbers<br />
CVV(number)--Must be 3 or 4 numbers<br />