/**
 * CRUD data Module
 */

//Dependancies
let fs = require('fs');
let path = require('path');
let config = require('./config');
let helpers = require('./helpers');

//Instantiate Data container
let lib = {};
//Base Directory of data
lib.baseDir = path.join(__dirname, `/../${config.dataDir}/`);

//Write Data to a file
lib.create = (dir, file, data, callback) => {
    fs.open(lib.baseDir + dir + '/' + file + '.json', 'wx', (err, fileDescriptor) => {
        if (!err && fileDescriptor) {
            //Convert The data to a string
            let stringData = JSON.stringify(data);
            //Write to file and close
            fs.writeFile(fileDescriptor, stringData, (err) => {
                if (!err) {
                    fs.close(fileDescriptor, (err) => {
                        if (!err) {
                            callback(false);
                        } else {
                            callback("Failed to close file");
                        }
                    })
                } else {
                    callback("Failed to write into the file");
                }
            })
        } else {
            callback("Could not create File, as It may already exist");
            console.log(err)
        }
    })
}

//Read Data from a file
lib.read = (dir, file, callback) => {
    fs.readFile(lib.baseDir + dir + '/' + file + '.json', 'utf8', (err, data) => {
        if (!err && data) {
            let parsedData = helpers.parseJsonToObject(data);
            callback(false, parsedData);
        } else {
            callback(err);
        }
    })
}

//Return All Data from Directory
lib.readAll = (dir, callback) => {
    let items = [];
    //Promise Array that will be used to resolve all promises
    let promises = [];
    fs.readdir(lib.baseDir + dir, (err, directoryData) => {
        if (!err) {
            //Push Promises to the array to resolve
            directoryData.forEach(element => {
                promises.push(
                    new Promise((resolve) => {
                        element = element.substring(0, element.indexOf('.'));
                        lib.read(dir, element, (err, fileData) => {
                            if (!err) {
                                items.push(fileData);
                                resolve(items);
                            }
                        })
                    })
                )
            })
            //Wait for all promises to finish in order to have correct Data.
            Promise.all(promises).then(() => {
                callback(false, items);
            })
        } else {
            callback(err)
        }
    })
}
// Update data in a file
lib.update = (dir, file, data, callback) => {

    // Open the file for writing
    fs.open(lib.baseDir + dir + '/' + file + '.json', 'r+', (err, fileDescriptor) => {
        if (!err && fileDescriptor) {
            // Convert data to string
            let stringData = JSON.stringify(data);

            // Truncate the file
            fs.ftruncate(fileDescriptor, (err) => {
                if (!err) {
                    // Write to file and close it
                    fs.writeFile(fileDescriptor, stringData, (err) => {
                        if (!err) {
                            fs.close(fileDescriptor, (err) => {
                                if (!err) {
                                    callback(false);
                                } else {
                                    callback('Error closing existing file');
                                }
                            });
                        } else {
                            callback('Error writing to existing file');
                        }
                    });
                } else {
                    callback('Error truncating file');
                }
            });
        } else {
            callback('Could not open file for updating, it may not exist yet');
        }
    });

};

lib.rename = (dir, oldfileName, newFileName, callback) => {
    fs.rename(lib.baseDir + dir + '/' + oldfileName + '.json', lib.baseDir + dir + '/' + newFileName + '.json', (err) => {
        if(!err){
            callback(false);
        }else{
            callback('Error Renaming')
        }
    })
}

//Delete a file
lib.delete = (dir, file, callback) => {
    //Unlinking the file (Removing)
    fs.unlink(lib.baseDir + dir + '/' + file + '.json', (err) => {
        if (!err) {
            callback(false);
        } else {
            callback('Error Deleting')
        }
    })
}


//Export Data Module
module.exports = lib;
