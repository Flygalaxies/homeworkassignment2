/**
 * The server main file
 */

//Dependencies
let http = require('http');
let url = require('url');
let StringDecoder = require('string_decoder').StringDecoder;
let handlers = require('./handlers')
let config = require('./config');
let helpers = require('./helpers');

//Initialize the server container
let server = {};


server.httpServer = http.createServer((req, res) => {
    server.mainServer(req, res);
})

//Server Logic
server.mainServer = (req, res) => {
    let path = url.parse(req.url, true).pathname.replace(/^\/+|\/+$/g, '');
    let queryString = url.parse(req.url, true).query;
    let method = req.method.toLowerCase();
    let headers = req.headers;
    let decoder = new StringDecoder('utf-8');
    let buffer = '';
    req.on('data', (data) => {
        buffer += decoder.write(data);
    })
    req.on('end', () => {
        buffer += decoder.end();

        //Check for mathcing router path, else notFound router
        let _chosenHandler = typeof (server.router[path]) !== 'undefined' ? server.router[path] : handlers.notFound;

        let data = {
            "queryString": queryString,
            "method": method,
            "headers": headers,
            "payload": helpers.parseJsonToObject(buffer)
        }
        _chosenHandler(data, (statusCode, payload) => {
            //Use payload from handler or else use empty object
            payload = typeof (payload) == 'object' ? payload : {};
            //Convert payload to String
            let payloadString = JSON.stringify(payload);
            //Return response       
            res.setHeader('Content-Type', 'application/json')
            res.writeHead(statusCode);
            res.end(payloadString);
        })
    })
}

//Define the request router
server.router = {
    "auth/register": handlers.authenticate.register,
    "auth/login": handlers.authenticate.login,
    "user": handlers.user,
    "items": handlers.items,
    // "items/add": handlers.items.addItem,
    // "items/get/single": handlers.items.getSingle,    
    "items/all": handlers.items.getAll,
    "checkout/addItem" : handlers.checkout.addItem,
    "checkout/pay" : handlers.checkout.pay,
}

server.init = () => {
    server.httpServer.listen(config.httpPort, () => {
        console.log(`Server is Listening on port ${config.httpPort}`);
    })
}
//Export the server
module.exports = server;