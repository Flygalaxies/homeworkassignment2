/**
 * The Helpers Module helps the main functions by providing extra functions that can be used by multiple main functions
 */

//Dependencies
let fs = require('fs');
let path = require('path');
let config = require('./config');
let crypto = require('crypto')

//Initiate the helpers container
let helpers = {};

//check if the specified directory exists, else create it
helpers.directoryCheck = (dir) => {
    let basePath = path.join(__dirname, `/../`);

    try {
        fs.statSync(basePath + `${dir}/`)
        return true;
    } catch (error) {
        if (error.code === 'ENOENT') {
            console.log("Directory does not exist, Creating Dirictory now");
            fs.mkdirSync(basePath + `${dir}`);
            return false;
        }
    }
}

//check if the specified file exists
helpers.fileCheck = (dir, file) => {
    let basePath = path.join(__dirname, `/../`);

    try {
        fs.statSync(basePath + `${dir}/${file}.json`)
        return true;
    } catch (error) {
        return false
    }
}

//Parse A JSON string to a object in all cases, without throwing
helpers.parseJsonToObject = (str) => {
    try {
        let obj = JSON.parse(str);
        return obj
    } catch (e) {
        return {};
    }
}

//Create a SHA256 hash
helpers.hash = (str) => {
    if (typeof (str) == 'string' && str.length > 0) {
        let encrypt = crypto.createHmac('sha256', config.hashingSecret).update(str).digest('hex');
        return encrypt;
    } else {
        return false;
    }
}

//Validate the SHA256 hashed password
helpers.validateHash = (str, hash) => {
    if (typeof (str) == 'string' && str.length > 0 && typeof (hash) == 'string' && hash.length == 64) {
        //Create a sha256 hash of the password and compare it to the hash stored
        let encrypt = crypto.createHmac('sha256', config.hashingSecret).update(str).digest('hex');
        if (encrypt === hash) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

//Create a random string
helpers.createRandomString = (strLength) => {
    strLength = typeof (strLength) == 'number' && strLength > 0 ? strLength : false;
    if (strLength) {
        //Define possible characters that could go into the string
        let possibleChars = 'abcdefghijklmnopqrstuvwxyz0123456789';
        //start the string
        let str = '';
        for (let i = 1; i <= strLength; i++) {
            //get a random character from possible characters
            let randomCharacter = possibleChars.charAt(Math.floor(Math.random() * possibleChars.length));
            //Append the character to possible string
            str += randomCharacter;
        }
        return str;
    } else {
        return false;
    }
}
//Validate if token is still valid
helpers.validateToken = (token) => {
    token = typeof (token) == 'object' ? token : false;
    if (token) {
        if (token.expiration > Date.now()) {
            return true;
        } else {
            return false;
        }
    }
}




//Export the helpers Module
module.exports = helpers;
