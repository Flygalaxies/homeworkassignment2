/**
 * External API Calls
*/

//Dependencies
let config = require("./config");
let https = require('https');
let queryString = require('querystring');
let _data = require("./data");

//Container
let extapi = {};

extapi.charge = (token, data, callback) => {
    let requestDetails = {
        protocol: 'https:',
        hostname: 'api.stripe.com',
        method: 'POST',
        path: '/v1/charges',
        auth: config.stripeApiKey,
    }
    let finalAmount = 0;
    let promises = [];
    data.forEach(element => {
        promises.push(
            new Promise((resolve) => {
                _data.read('items', element.name, (err, itemData) => {
                    if (!err && itemData) {
                        finalAmount = finalAmount + itemData.amount;
                    }
                    resolve(finalAmount);
                })
            })
        )

    });
    Promise.all(promises).then(() => {
        let charge = {
            'amount': finalAmount,
            'currency': 'usd',
            'source': token.id,
            'description': 'Test Charge'
        }
        let stringCharge = queryString.stringify(charge);
        let request = https.request(requestDetails, res => {
            let data = '';
            res.on('data', chunk => {
                data += chunk;
            })
            res.on('end', () => {
                callback(JSON.parse(data))
            })
        })
        request.on('error', err => {
            console.log(err);
        })
        request.write(stringCharge);
        request.end();
    })
}

extapi.createToken = (card, callback) => {
    let requestDetails = {
        protocol: 'https:',
        hostname: 'api.stripe.com',
        method: 'POST',
        path: '/v1/tokens',
        auth: config.stripeApiKey,
    }
    let cardString = queryString.stringify(card);
    let request = https.request(requestDetails, res => {
        let data = '';
        res.on('data', chunk => {
            data += chunk;
        })
        res.on('end', () => {
            callback(JSON.parse(data));
        })
    })
    request.on('error', err => {
        console.log(err);
    })
    request.write(cardString);
    request.end();
}

extapi.sendmail = (mail, callback) => {
    let requestDetails = {
        protocol: 'https:',
        hostname: 'api.mailgun.net',
        method: 'POST',
        path: `/v3/${config.mailgun.domain}/messages`,
        auth: config.mailgun.apiKey,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }
    let mailString = queryString.stringify(mail);
    let request = https.request(requestDetails, res => {
        let data = '';
        res.on('data', chunk => {
            data += chunk;
        })
        res.on('end', () => {
            callback(data);
        })
    })
    request.on('error', (err) => {
        console.log(err);
    })
    request.write(mailString);
    request.end();
}

//Export Module
module.exports = extapi;