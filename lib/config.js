/**
 * Configuration variables
 */

//Environment container
let environments = {};

//Create staging(Development) enironment
environments.staging = {
    'httpPort': 4200,
    'dataDir': '.data',
    'hashingSecret': 'ThisIsHashingSecret',
    'tokenLength': 64,
    'stripeApiKey': 'sk_test_4eC39HqLyjWDarjtT1zdp7dc',
    'mailgun': {
        'domain': 'sandbox5f06548e93e54d61a0ce9c877d371d37.mailgun.org',
        'from': 'ian@sandbox123.mailgun.org',
        'apiKey': `api:7e4ecb02a3b46e0ea3f981f8ffdf7ba6-52cbfb43-5ad79977`
    }
}


//Export Environment
module.exports = environments.staging;