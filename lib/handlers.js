/**
 * Handlers and routing Methods
 */

//Dependancies
let helpers = require('./helpers');
let auth = require('./handlers/auth');
let items = require('./handlers/items');
let checkout = require('./handlers/checkout');
let user = require('./handlers/user');
//Handler container
let handlers = {};

//Authentication Submethod container
handlers.authenticate = {};

//Route to Authentication module
handlers.authenticate.register = (data, callback) => {
    let acceptableMethods = ['post'];
    if (acceptableMethods.indexOf(data.method) > -1) {
        auth.register(data, callback);
    } else {
        callback(405);
    }
}
handlers.authenticate.login = (data, callback) => {
    let acceptableMethods = ['post'];
    if (acceptableMethods.indexOf(data.method) > -1) {
        auth.login(data, callback);
    } else {
        callback(405);
    }
}

//User Submethod Container
handlers.user = {}
//Route to user module
handlers.user = (data, callback) => {
    let acceptableMethods = ['get', 'put', 'delete'];
    if (acceptableMethods.indexOf(data.method) > -1) {
        user[data.method](data, callback);
    } else {
        callback(405);
    }
}


//Items Submethod Container
handlers.items = {};
//Route to Items module
handlers.items = (data, callback) => {
    let acceptableMethods = ['post', 'get', 'put', 'delete'];
    if (acceptableMethods.indexOf(data.method) > -1) {
        // handlers._users[data.method](data, callback);
        items[data.method](data, callback);
    } else {
        callback(405);
    }
};
handlers.items.getAll = (data, callback) => {
    let acceptableMethods = ['get'];
    if (acceptableMethods.indexOf(data.method) > -1) {
        items.all(data, callback);
    } else {
        callback(405);
    }
}

//Checkout Submethod Container
handlers.checkout = {}
//Route to checkout module
handlers.checkout.addItem = (data, callback) => {
    let acceptableMethods = ['post', 'get', 'put', 'delete'];
    if (acceptableMethods.indexOf(data.method) > -1) {
        checkout.addItem(data, callback);
    } else {
        callback(405);
    }
}
handlers.checkout.pay = (data, callback) => {
    let acceptableMethods = ['post', 'get', 'put', 'delete'];
    if (acceptableMethods.indexOf(data.method) > -1) {
        checkout.pay(data, callback);
    } else {
        callback(405);
    }
}


handlers.notFound = (data, callback) => {
    callback(404);
}

//Export the handler
module.exports = handlers;