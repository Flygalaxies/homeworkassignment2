/**
 *  CHECKOUT MODULE
 */

//Dependencies
let helpers = require('../helpers');
let _data = require('../data');
let config = require('../config');
let extapi = require('../extAPI')

//Checkout Container
let checkout = {};

//REQUIRED: QUERY(tokenId: tokenID)
//          PAYLOAD([item: item])
checkout.addItem = (data, callback) => {
    let tokenID = typeof (data.queryString.tokenId) == 'string' && data.queryString.tokenId.trim().length == config.tokenLength ? data.queryString.tokenId.trim() : false;
    let names = [];
    data.payload.forEach(element => {
        names.push(element);
    })
    if (tokenID) {
        _data.read('tokens', tokenID, (err, tokenData) => {
            if (!err && helpers.validateToken(tokenData)) {
                helpers.directoryCheck(`${config.dataDir}/checkout`);
                if (helpers.fileCheck(`${config.dataDir}/checkout`, tokenData.user.email)) {
                    _data.delete('checkout', tokenData.user.email, () => {
                        console.log("Deleted file");
                        _data.create('checkout', tokenData.user.email, names, (err) => {
                            if (!err) {
                                callback(200)
                            } else {
                                callback(500, { 'Error': "Could not create checkout" })
                            }
                        })
                    });
                } else {
                    _data.create('checkout', tokenData.user.email, names, (err) => {
                        if (!err) {
                            callback(200);
                        } else {
                            callback(500, { 'Error': "Could not create checkout" })
                        }
                    })
                }
            } else {
                callback(401, { 'Error': "Unauthorised" })
            }
        })
    } else {
        callback(400, { 'Error': "Invalid Parameters" })
    }
}

//REQUIRED: PAYLOAD: {cardNumber: cardnumber, expMonth: expMonth, expYear: expYear, CVV: cvv}
//          QUERY: tokenId: userTokenId, email: userEmail
//
checkout.pay = (data, callback) => {
    let userTokenID = typeof (data.queryString.tokenId) == 'string' && data.queryString.tokenId.trim().length == config.tokenLength ? data.queryString.tokenId.trim() : false;
    let email = typeof (data.queryString.email) == 'string' && data.queryString.email.trim().length > 0 ? data.queryString.email.trim() : false;
    let cardNumber = typeof (data.payload.cardNumber) == 'number' && data.payload.cardNumber.toString().length == 16 ? data.payload.cardNumber : false;
    let expMonth = typeof (data.payload.expMonth) == 'number' && data.payload.expMonth.toString().length == 2 ? data.payload.expMonth : false;
    let expYear = typeof (data.payload.expYear) == 'number' && data.payload.expYear.toString().length == 4 ? data.payload.expYear : false;
    let CVV = typeof (data.payload.CVV) == 'number' && (data.payload.CVV.toString().length == 3 || data.payload.CVV.toString().length == 4) ? data.payload.CVV : false;
    if (userTokenID && email) {
        if (cardNumber && expMonth && expYear && CVV) {
            _data.read('checkout', email, (err, checkoutData) => {
                if (!err && checkoutData) {
                    console.log(checkoutData);
                    let card = {
                        'card[number]': cardNumber,
                        'card[exp_month]': expMonth,
                        'card[exp_year]': expYear,
                        'card[cvc]': CVV
                    }
                    extapi.createToken(card, token => {
                        console.log(token);
                        extapi.charge(token, checkoutData, paymentCallback => {
                            console.log(paymentCallback);
                            let mail = {
                                from: config.mailgun.from,
                                'to': email,
                                'to': 'iantolmay@mweb.co.za',
                                'subject': "ORDER CONFIRMATION",
                                'text': `Your Order Totals to: ${paymentCallback.amount}`
                            }
                            extapi.sendmail(mail, mailCallback => {
                                console.log(mailCallback);
                                callback(200);
                            });
                        });
                    });
                } else {
                    callback(500, { "Error": "Failed to read checkout data" })
                }
            })
        } else {
            callback(400, { "Error": "Invalid Parameters" })
        }
    } else {
        callback(401, { "Error": "Unauthorised, please login." })
    }
}


//Export module
module.exports = checkout;