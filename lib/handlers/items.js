/**
 * MENU ITEMS Module
 */

//Dependencies
let helpers = require('../helpers');
let _data = require('../data');
let config = require('../config');

//Items Module Container
let items = {};
items.get = {};

//Add item
//REQUIRED: QUERY(tokenId: tokenID)
//          PAYLOAD(item: itemName, itemDescription, amount)
items.post = (data, callback) => {
    let tokenID = typeof (data.queryString.tokenId) == 'string' && data.queryString.tokenId.trim().length == config.tokenLength ? data.queryString.tokenId.trim() : false;
    let itemName = typeof (data.payload.name) == 'string' && data.payload.name.trim().length > 0 ? data.payload.name.trim() : false;
    let itemDescription = typeof (data.payload.description) == 'string' && data.payload.description.trim().length > 0 ? data.payload.description.trim() : false;
    let amount = typeof (data.payload.amount) == 'number' && data.payload.amount >= 0 ? data.payload.amount : false;
    if (tokenID) {
        if (itemName && itemDescription) {
            _data.read('tokens', tokenID, (err, data) => {
                if (!err) {
                    if (data.user.isAdmin && helpers.validateToken(data)) {
                        helpers.directoryCheck(`${config.dataDir}/items`);
                        let item = {
                            'name': itemName,
                            'description': itemDescription,
                            'amount': amount
                        }
                        _data.read('items', itemName, (err, itemData) => {
                            if (err) {
                                _data.create('items', item.name, item, (err) => {
                                    if (!err) {
                                        callback(200);
                                    } else {
                                        callback(500, { 'Error': 'Could not create item' })
                                    }
                                })
                            } else {
                                callback(400, { 'Error': "Could not create item, it may already exist" })
                            }
                        })
                    } else {
                        callback(401, { 'Error': "Not Authenticated" })
                    }
                } else {
                    callback(500, { 'Error': "Failed to open file" })
                }
            })
        } else {
            callback(400, { 'Error': "Invalid payload" })
        }
    } else {
        callback(400, { 'Error': "Invalid query Parameter" })
    }
}

//Get all items
//REQUIRED: QUERY(none)
//          PAYLOAD(none)
items.all = (data, callback) => {
    let tokenID = typeof (data.queryString.tokenId) == 'string' && data.queryString.tokenId.trim().length == config.tokenLength ? data.queryString.tokenId.trim() : false;
    if (tokenID) {
        _data.readAll('items', (err, data) => {
            if (!err) {
                callback(200, data);
            } else {
                callback(500, { 'Error': "Could not retrieve Data" });
            }
        });
    } else {
        callback(400, { 'Error': "Invalid Query Parameter" })
    }
}
//Get Single items
//REQUIRED: QUERY(tokenId: tokenID, item: itemName)
//          PAYLOAD(none)
items.get = (data, callback) => {
    let tokenID = typeof (data.queryString.tokenId) == 'string' && data.queryString.tokenId.trim().length == config.tokenLength ? data.queryString.tokenId.trim() : false;
    let name = typeof (data.queryString.itemName) == 'string' && data.queryString.itemName.trim().length > 0 ? data.queryString.itemName.trim() : false;
    if (tokenID && name) {
        _data.read('tokens', tokenID, (err, data) => {
            if (!err) {
                if (helpers.validateToken(data)) {
                    _data.read('items', name, (err, itemData) => {
                        if (!err) {
                            callback(200, itemData);
                        } else {
                            callback(500, { 'Error': 'Failed to open File' })
                        }
                    })
                } else {
                    callback(401, { 'Error': "Not Authenticated" });
                }
            } else {
                callback(500, { 'Error': "Could not open file" })
            }
        })
    } else {
        callback(400, { 'Error': "Invalid Query parameters" })
    }
}
//Update Item
//REQUIRED Query: tokenID, itemName
//OPTIONAL payload: name, description, amount (ONE REQUIRED)
items.put = (data, callback) => {
    let tokenID = typeof (data.queryString.tokenId) == 'string' && data.queryString.tokenId.trim().length == config.tokenLength ? data.queryString.tokenId.trim() : false;
    let queryName = typeof (data.queryString.itemName) == 'string' && data.queryString.itemName.trim().length > 0 ? data.queryString.itemName.trim() : false;
    let name = typeof (data.payload.name) == 'string' && data.payload.name.trim().length > 0 ? data.payload.name.trim() : false;
    let description = typeof (data.payload.description) == 'string' && data.payload.description.trim().length > 0 ? data.payload.description.trim() : false;
    let amount = typeof (data.payload.amount) == 'number' && data.payload.amount.trim().length > 0 ? data.payload.amount.trim() : false;
    if (tokenID && queryName && (name || description || amount)) {
        _data.read('tokens', tokenID, (err, tokenData) => {
            if (!err && tokenData) {
                if (helpers.validateToken(tokenData) && tokenData.user.isAdmin == true) {
                    _data.read('items', queryName, (err, itemData) => {
                        if (!err && itemData) {
                            if (name) {
                                itemData.name = name;
                            }
                            if (description) {
                                itemData.description = description;
                            }
                            if (amount) {
                                itemData.amount = amount;
                            }
                            _data.rename('items', queryName, name, (err) => {
                                if (!err) {
                                    _data.update('items', name, itemData, (err) => {
                                        if (!err) {
                                            callback(200);
                                        } else {
                                            callback(500, { "Error": "Could not update file" })
                                        }
                                    })
                                } else {
                                    callback(500, { "Error": "Could not update file " })
                                }
                            })

                        } else {
                            callback(500, { "Error": "Could not find item" })
                        }
                    })
                } else {
                    callback(401, { "Error": "Unauthorised, please login" })
                }
            } else {
                callback(500, { "Error": "Could not find token" })
            }
        })
    }
}
//DELETE item
//REquired : query: tokenID, itemName
items.delete = (data, callback) => {
    let tokenID = typeof (data.queryString.tokenId) == 'string' && data.queryString.tokenId.trim().length == config.tokenLength ? data.queryString.tokenId.trim() : false;
    let name = typeof (data.queryString.itemName) == 'string' && data.queryString.itemName.trim().length > 0 ? data.queryString.itemName.trim() : false;
    if (tokenID && name) {
        _data.read('tokens', tokenID, (err, tokenData) => {
            if (!err && tokenData) {
                if (helpers.validateToken(tokenData) && tokenData.user.isAdmin == true) {
                    _data.delete('items', name, (err) => {
                        if (!err) {
                            callback(200);
                        } else {
                            callback(500, "Failed to delete item")
                        }
                    })
                } else {
                    callback(401, { "Error": "Unauthorised" })
                }
            } else {
                callback(500, { "Error": " Could not find token" })
            }
        })
    } else {
        callback(400, { "Error": "Invalid parameters" })
    }
}


//Export Module
module.exports = items;
