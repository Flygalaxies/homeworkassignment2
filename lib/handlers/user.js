/**
 * USER Module
 */

//Dependencies
let helpers = require('../helpers');
let _data = require('../data');
let config = require('../config');

//User Module Container
let user = {};

//REQUIRED : Query: tokenId
user.get = (data, callback) => {
    let tokenID = typeof (data.queryString.tokenId) == 'string' && data.queryString.tokenId.trim().length == config.tokenLength ? data.queryString.tokenId.trim() : false;
    if (tokenID) {
        _data.read('tokens', tokenID, (err, tokenData) => {
            if (!err && tokenData) {
                if (helpers.validateToken(tokenData)) {
                    _data.read('users', tokenData.user.email, (err, userData) => {
                        if (!err && userData) {
                            callback(200, userData);
                        } else {
                            callback(500, { "Error": "Could not find user" })
                        }
                    })
                } else {
                    callback(401, { "Error": "Not authorised, Please log in" })
                }
            } else {
                callback(500, { "Error": "Failed to read token" })
            }
        })
    } else {
        callback(400, { "Error": "Invalid Parameters" });
    }
}
//Update User
//REQUIRED: Query: tokenId,
//Optional: PAYLOAD: name, address, password (AT LEAST ONE)
user.put = (data, callback) => {
    let tokenID = typeof (data.queryString.tokenId) == 'string' && data.queryString.tokenId.trim().length == config.tokenLength ? data.queryString.tokenId.trim() : false;
    let name = typeof (data.payload.name) == 'string' && data.payload.name.trim().length > 0 ? data.payload.name.trim() : false;
    let address = typeof (data.payload.address) == 'string' && data.payload.address.trim().length > 0 ? data.payload.address.trim() : false;
    let password = typeof (data.payload.password) == 'string' && data.payload.password.trim().length > 0 ? data.payload.password.trim() : false;
    if (tokenID && (name || address || password)) {
        _data.read('tokens', tokenID, (err, tokenData) => {
            if (!err && tokenData) {
                _data.read('users', tokenData.user.email, (err, userData) => {
                    if (!err && userData) {
                        if (name) {
                            userData.name = name;
                        }
                        if (address) {
                            userData.address = address;
                        }
                        if (password) {
                            let hashedPassword = helpers.hash(password);
                            userData.password = hashedPassword;
                        }
                        _data.update('users', tokenData.user.email, userData, err => {
                            if (!err) {
                                callback(200);
                            } else {
                                callback(500, { "Error": "Failed to update" })
                            }
                        })
                    } else {
                        callback(500, { "Error": "Could not find user" })
                    }
                })
            } else {
                callback(500, { "Error": "Failed to read token" })
            }
        })
    } else {
        callback(400, { "Error": "Invalid Parameters" });
    }
}
//Delete User
//REQUIRED: Query: tokenId,
user.delete = (data, callback) => {
    let tokenID = typeof (data.queryString.tokenId) == 'string' && data.queryString.tokenId.trim().length == config.tokenLength ? data.queryString.tokenId.trim() : false;
    if (tokenID) {
        _data.read('tokens', tokenID, (err, tokenData) => {
            if (!err && tokenData) {
                if (helpers.validateToken(tokenData)) {
                    _data.delete('users', tokenData.user.email, (err) => {
                        if (!err) {
                            callback(200);
                        } else {
                            callback(500, { "Error": "Failed to delete user" })
                        }
                    })
                } else {
                    callback(401, "Unauthorised, please login")
                }
            } else {
                callback(500, { "Error": "Could not find token" });
            }
        })
    } else {
        callback(400, { "Error": "Invalid Parameters" })
    }
}

//Export Module
module.exports = user;
