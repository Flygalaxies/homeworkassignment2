/**
 * Authentication Module
 */

//Dependencies
let _data = require('../data');
let config = require('../config');
let helpers = require('../helpers');
let token = require('../token');

//Instantiate the authentication container
let auth = {};

//Instantiate the submethods for Container
auth.login = {};
auth.register = {};

//Required Data: email, password, name, address isAdmin
auth.register = (data, callback) => {
    let email = typeof (data.payload.email) == 'string' && data.payload.email.trim().length > 0 ? data.payload.email.trim() : false;
    let password = typeof (data.payload.password) == 'string' && data.payload.password.trim().length > 0 ? data.payload.password.trim() : false;
    let name = typeof (data.payload.name) == 'string' && data.payload.name.trim().length > 0 ? data.payload.name.trim() : false;
    let address = typeof (data.payload.address) == 'string' && data.payload.address.trim().length > 0 ? data.payload.address.trim() : false;
    let isAdmin = typeof (data.payload.isAdmin) == 'boolean' ? data.payload.isAdmin : false;
    if (email && name && address && isAdmin && password) {
        helpers.directoryCheck(`${config.dataDir}/users`);
        _data.read('users', email, (err) => {
            if (err) {
                let hahedPassword = helpers.hash(password);
                let userObject = {
                    'email': email,
                    'password': hahedPassword,
                    'name': name,
                    'address': address,
                    'isAdmin': isAdmin
                }
                _data.create('users', email, userObject, (err) => {
                    if (!err) {
                        callback(200);
                    } else {
                        callback(500, "A user could not be created");                        
                    }
                })
            } else {
                callback(400, 'User already exists');
            }
        })
    } else {
        callback(400, { "Error": "Missing required fields" })
    }
}

//Required Data: Email and Password
auth.login = (data, callback) => {
    let email = typeof (data.payload.email) == 'string' && data.payload.email.trim().length > 0 ? data.payload.email.trim() : false;
    let password = typeof (data.payload.password) == 'string' && data.payload.password.trim().length > 0 ? data.payload.password.trim() : false;
    if (email && password) {
        _data.read('users', email, (err, data) => {
            if (!err) {
                if (email === data.email && helpers.validateHash(password, data.password)) {
                    //Create token Valid for 1 hour
                    let tokenID = token.createToken(1000 * 60 * + 60, data);
                    _data.read('tokens', tokenID, (err, tokenData) => {
                        if (!err) {                            
                            callback(200, tokenData);
                        } else {
                            callback(500, { "Error": "Failed to open file" })
                        }
                    })
                } else {
                    callback(401, { "Error": "Email or Password is incorrect" })
                }
            } else {
                callback(400, { "Error": "Email or Password is incorrect" });
            }
        })
    } else {
        callback(400, { 'Error': "Missing required fields" });
    }
}

//Export the Authentication module
module.exports = auth;
