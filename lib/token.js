/**
 * CREATE AND GET TOKENS
 */

//Dependencies
let config = require('./config');
let _data = require('./data');
let helpers = require('./helpers');

//Create Token Module container
let tokens = {}

//Create a token With the user inside
tokens.createToken = (expireSeconds, userObject) => {
    let user = typeof (userObject) == 'object' ? userObject : false;
    if (user) {
        let tokenID = helpers.createRandomString(config.tokenLength);
        let expiration = Date.now() + expireSeconds;
        let token = {
            "user": user,
            "id": tokenID,
            "expiration": expiration
        }
        //Check if the directory existsm and add the token to it
        helpers.directoryCheck(`${config.dataDir}/tokens`);
        _data.create('tokens', tokenID, token, (err) => {
            if (!err) {
                return true;
            } else {
                return false;
            }
        })
        return tokenID;
    } else {
        return false
    }
}



//Export module
module.exports = tokens;